import _new from 'aktivitet/new';
import load from 'aktivitet/load';
import globals from 'aktivitet/globals';
import selectors from 'aktivitet/selectors';

selectors.BUTTON_NAVIGATE = '[data-navigate]';

class ActivityMap extends Map
{
    constructor( map = {}, options = {} )
    {
        if ( map instanceof Object )
            map = Object.entries( map );

        super( map );

        this.options = Object.assign( {
            base_type: 'page',
            event_listener: false
        }, options );

        this.load_activities();

        this.current = null;

        if ( this.options.event_listener )
            globals.$event_root.addEventListener( 'click', this.handle_button_click.bind( this ) );
    }

    handle_button_click( e )
    {
        var url;

        if ( e.target.matches( selectors.BUTTON_CLICK_ELS ) && e.target.matches( selectors.BUTTON_NAVIGATE ) )
        {
            e.preventDefault();

            if ( e.target.attributes[ 'href' ] )
                url = e.target.attributes[ 'href' ].value;
            else
                url = e.target.dataset.navigate;

            this.navigate( url );
        }
    }

    load_activities()
    {
        this.forEach( function ( child_map, aid, map ) {
            var $activity, options = {}, load_state;

            // require activity type key
            if ( child_map.activity === undefined )
                child_map.activity = 'activity';

            // create activity element
            $activity = document.createElement( 'activity' );
            $activity.dataset.aid = aid;
            $activity.dataset.activity = child_map.activity;

            // store and remove any options
            if ( child_map.options !== undefined )
            {
                options = Object.assign( options, child_map.options );

                delete child_map.options;
            }

            // remove activity key
            delete child_map.activity;

            // anything left passed to child map
            child_map = new ActivityMap( child_map );

            // append child els
            child_map.forEach( function ( child_map, aid, map ) {
                $activity.append( child_map.activity.$activity );
            } );

            load_state = load( $activity, options );
            child_map.activity = _new( $activity, options, load_state );
            map.set( aid, child_map );
        } );
    }

    get( key )
    {
        var ret = this;

        for( const k of key.split( /\// ) )
        {
            ret = Map.prototype.get.call( ret, k );
            if ( ! ret )
                return;
        }

        return ret;
    }

    navigate( path, load = true )
    {
        var current = this.current;

        if ( current )
        {
            switch ( path )
            {
                case 'parent':
                    current = current.activity.query( path );
                break;
                case 'next':
                case 'previous':
                case 'next-sibling':
                case 'previous-sibling':
                    current = current.activity.query( path );
                    if ( current.length )
                        current = current[0];
                    else
                        current = null;
                break;
                default:
                    current = this.get( path );
            }
        }
        else
            current = this.get( path );

        if ( current )
            this.current = current;
        else
            throw "path not found: " + path;

        if ( load )
            this.current.activity.load_url();

        return this.current;
    }
}

export default ActivityMap;